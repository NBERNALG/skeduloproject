import React from 'react';
import { Linking, StyleSheet, TouchableOpacity } from 'react-native';

import { Text, View } from '../components/Themed';

/** screen to show the content of the tab two */
export default function TabTwoScreen() {
  /* conatins all the elements to validate */
  let [unavailableItems, onChangeUnavailableItems] = React.useState([
    { startPx: 10, endPx: 30 },
    { startPx: 55, endPx: 65 },
    { startPx: 35, endPx: 50 },
    { startPx: 20, endPx: 40 },
    { startPx: 60, endPx: 70 },
  ]);
  /* items to delete */
  const deleteItems: any = [];
  /** indicates if the calculated button was pressed */
  let [calculated, onChangeCalculated] = React.useState(false);
  /* repo link */
  var link = <Text style={styles.link} onPress={() => Linking.openURL('https://gitlab.com/NBERNALG/skeduloproject/-/blob/developer/screens/TabTwoScreen.tsx')}>screens/TabTwoScreen.tsx</Text>

  return (
    <View style={styles.container}>
      <Text style={styles.screenTitle}>View the {link} to show the code in ES6 in action</Text>
      <Text>Open the web inspector to view after and before process log</Text>
      {/* calculate button */}
      {!calculated && <TouchableOpacity
        onPress={calculateOverlapping}
        style={styles.searchButton}
        accessibilityLabel="filter the content with this white button"
      >
          <Text style={styles.searchButtonText}>Calculate</Text>
      </TouchableOpacity>}
      {/* unavailable items */}
      { unavailableItems.map((item: any, index: number) => <Text key={index} style={styles.item}>startPx: { item.startPx } endPx: { item.endPx }</Text>) }

    </View>
  );

  /** test the overlapping items */
  function calculateOverlapping() {
    for (let i = 0; i < unavailableItems.length; i++) {
      isOverlapping(unavailableItems[i], i);
    }
    console.log('original data', unavailableItems);
    /* delete overlapped items */
    for (let i = deleteItems.length -1; i >= 0; i--) {
      unavailableItems.splice(deleteItems[i], 1);
    }
    onChangeCalculated(true);
    onChangeUnavailableItems(unavailableItems)
    console.log('result data', unavailableItems);
  }

  /** evaluate the overlape of an item */
  function isOverlapping(item: any, index: number): boolean {
    let deleteItem = false;
    for (let i = 0; i < unavailableItems.length; i++) {
      /* validate is item is being to delete */
      if (deleteItems.includes(i)) {
        continue;
      }
      // if is the same element
      if (item.startPx === unavailableItems[i].startPx && item.endPx === unavailableItems[i].endPx) {
        continue;
      }
      // if contains in start
      if (item.startPx < unavailableItems[i].startPx && item.endPx >= unavailableItems[i].startPx) {
        unavailableItems[i].startPx = item.startPx;
        deleteItem = true;
        continue;
      }
      // if conatins in end
      if (item.startPx <= unavailableItems[i].endPx && item.endPx > unavailableItems[i].endPx) {
        unavailableItems[i].endPx = item.endPx;
        deleteItem = true;
        continue;
      }
    }
    // delete the item in unavailable
    if (deleteItem) {
      deleteItem = true;
      deleteItems.push(index)
    }
    return deleteItem;
  }
  
}

/* screen styles */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  /* screen title */
  screenTitle: {
    fontSize: 24,
    margin: 10
  },
  link: {
    textDecorationLine: 'underline',
    color: 'blue'
  },
  /* search button */
  searchButton: {
    height: 40,
    width: 80,
    borderColor: 'gray',
    borderWidth: 1,
    padding: 5,
    borderRadius: 5,
    margin: 30,
    textAlign: 'center',
    backgroundColor: '#FFF',
    borderEndColor: '#0461cb',
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center'
  },
  /* search button text */
  searchButtonText: {
      color: '#0461cb',
      fontWeight: 'bold',
      textAlign: 'center'
  },
  /* item */
  item: {
    fontSize: 18,
    marginTop: 40
  }
});
