import { ScrollView, StyleSheet } from 'react-native';
import WelcomeMessage from '../components/WelcomeMessage';
import Search from '../components/Search';
import React from 'react';

/** screen to show the content of the tab one */
export default function TabOneScreen() {

  return (
    <ScrollView style={styles.container}>
      {/* welcome message component */}
      <WelcomeMessage/>
      {/* search component */}
      <Search/>
    </ScrollView>
  );
}

/* screen styles */
const styles = StyleSheet.create({
  /* conatiner */
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFF'
  }
});
