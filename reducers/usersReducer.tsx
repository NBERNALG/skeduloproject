/* data type */
export interface UsersState {
    users: any[];
    loading: boolean;
    lastSearch: string;
  }
  /* initial state if no data */
  const initialState = {
    users: [],
    loading: false,
    lastSearch: ''
  };
  /* define type for data transfer */
  export type Action = { type: 'new_search'; payload: any };
  
  /* reducer logic */
  export const usersReducer = (
    state: UsersState = initialState,
    action: Action
  ) => {
    switch (action.type) {
      case 'new_search': {
        return { 
          users: action.payload.users ,
          loading: action.payload.loading,
          lastSearch: action.payload.lastSearch
        };
      }
      default:
        return state;
    }
  };