
import React from 'react';
import { StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';
import GithubUserCard from './GithubUserCard';
import { useSelector, useDispatch } from "react-redux";
import { Text, View } from './Themed';
import { UsersState } from '../reducers/usersReducer';

export default function Search() {
    /* searched text */
    let [searchText, onChangeSearchText] = React.useState('');
    /* list of users */
    const users = useSelector<UsersState, UsersState['users']>(
        (state) => state.users
    );
    /** indicates if the component is loading */
    let loading = useSelector<UsersState, UsersState['loading']>(
        (state) => state.loading
    )
    /** indicates if the component is loading */
    let lastSearch = useSelector<UsersState, UsersState['lastSearch']>(
        (state) => state.lastSearch
    )
    const dispatch = useDispatch();

    /* return */
    return (
        <View style={styles.container}>
            <View style={styles.searcherContainer}>
                {/* description message */}
                <Text
                style={styles.getStartedText}>
                Write something to filter users from the Github Users API
                </Text>

                <View style={styles.searchBarContainer}>
                    {/* search input */}
                    <TextInput style={styles.searchFilter} placeholder="Type text here..."
                    placeholderTextColor={Colors.primaryText.text}
                    onChangeText={onChangeSearchText}
                    onKeyPress={handleEnterSearch}
                    autoFocus
                    value={searchText}>
                    </TextInput>

                    {/* search button */}
                    {searchText.length > 2 && <TouchableOpacity
                    onPress={handleSearch}
                    style={styles.searchButton}
                    accessibilityLabel="filter the content with this white button"
                    disabled={searchButtonIsValid()}
                    >
                        <Text style={styles.searchButtonText}>Search</Text>
                    </TouchableOpacity>}
                </View>
            </View>

             {/* finded users */}
             {!loading && users.length > 0 && <React.StrictMode>
                <Text style={styles.resultsNumber}>{users.length} results found with query {lastSearch}</Text>
                <Text style={styles.resultsNumber}>click them to open profile</Text>
                <View style={styles.githubUsersContainer} >
                    { users.map((user: any) => <GithubUserCard props={{user: user}} key={user.id}/>) }
                </View>
            </React.StrictMode>}

            {/* loading */}
            {loading && <React.StrictMode>
                <Text style={styles.resultsNumber}>loading...</Text>
                <View style={styles.githubUsersContainer} >
                        { [1,2,3,4,5].map( (number: number) => <View key={number} style={styles.skeleton}></View>) }
                </View>
            </React.StrictMode>}

            {/* zero state */}
            {!loading && users.length === 0 && <React.StrictMode>
                <View style={styles.githubUsersContainer} >
                    <Text style={styles.resultsNumber}>Type at search box at least 3 characters to make a query</Text>
                </View>
            </React.StrictMode>}

        </View>
    );

    /* make the search in the Github Api */
    function handleSearch() {
        /* update redux state */
        dispatch({ type: 'new_search', payload: {
            users: [],
            loading: true,
            lastSearch: searchText
        }});
        /* get the results from the server */
        fetch(`https://api.github.com/search/users?q=${searchText}&per_page=100`, {
            method: 'GET',
            headers: {
                Accept: 'application/vnd.github.v3+json'
            }
        })
        .then((response) => response.json())
        .then((json) => {
            if (json.items.length === 0) {
                alert('no results found')
            }
            /* update redux state */
            dispatch({ type: 'new_search', payload: {
                users: json.items,
                loading: false,
                lastSearch: searchText
            }});
            /* clear the searxh field */
            onChangeSearchText('')
        })
        .catch((error) => {
            console.error(error);
        });
    }


    /* validate is search text is big than 2 */
    function searchButtonIsValid() {
        if (searchText.length < 3 || loading) {
            return true
        }
        return false;
    }

    /* reaction to Enter keyboard event to make a search */
    function handleEnterSearch(event: any) {
		if (event.key === 'Enter' && searchText.length > 2) {
			handleSearch();
        }
    }
}

/* component styles */
const styles = StyleSheet.create({
    /* component container */
    container: {
        width: '100%',
    },
    /* searcher container */
    searcherContainer: {
        backgroundColor: Colors['primaryText'].text,
        width: '100%',
        alignItems: 'center',
        paddingBottom: 10
    },
    /** search bar */
    searchBarContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: Colors['primaryText'].text,
        width: '100%',
        alignItems: 'center',
        paddingBottom: 10
    },
    /* welcome message */
    getStartedText: {
        fontSize: 17,
        lineHeight: 24,
        textAlign: 'center',
        color: Colors['whiteText'].text,
        paddingHorizontal: 10
    },
    helpContainer: {
        marginTop: 15,
        marginHorizontal: 20,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        textAlign: 'center',
    },
    /* search text input */
    searchFilter: {
        height: 60,
        width: 250,
        borderColor: Colors.whiteText.text,
        color: Colors.primaryText.text,
        backgroundColor: '#FFF',
        borderWidth: 1,
        padding: 15,
        borderRadius: 20,
        margin: 10,
        textAlign: 'center'
    },
    /* search button */
    searchButton: {
        height: 40,
        width: 80,
        borderColor: 'gray',
        borderWidth: 1,
        padding: 5,
        borderRadius: 5,
        margin: 5,
        textAlign: 'center',
        backgroundColor: '#FFF',
        borderEndColor: '#0461cb',
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center'
    },
    /* search button text */
    searchButtonText: {
        color: '#0461cb',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    /* number of results found in the query */
    resultsNumber: {
        marginTop: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    /** github users container */
    githubUsersContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        backgroundColor: '#FFF',
        paddingTop: 20,
        marginTop: 10,
        textAlign: 'center',
        paddingHorizontal: 20
    },
    /* skeleton div */
    skeleton: {
        width: 220,
        height: 80,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#c1c1c19c',
        borderRadius: 10,
        margin: 10,
        padding: 10,
    }
});
