import { Text, TextProps } from './Themed';

/* define base styles for space-mono font */
export function MonoText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />;
}
