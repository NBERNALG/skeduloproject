
import React from 'react';
import { Image, Pressable, StyleSheet } from 'react-native';
import { Text, View } from './Themed';
import * as WebBrowser from 'expo-web-browser';

/** component to show a github user */
export default function GithubUserCard({props}: any) {
    return (
        <View>
            <Pressable onPress={openGithubProfile} style={styles.githubUserCard}>
                {/* avatar wrapper */}
                <View style={styles.avatarContainer}>
                    <Image
                        style={styles.avatar}
                        source={{
                            uri: props.user.avatar_url,
                        }}
                    />
                    
                </View>
                {/*  */}
                <View style={styles.infoContainer}>
                    <Text style={styles.userLogin}>{props.user.login}</Text>
                    <Text style={styles.userType}>{props.user.type}</Text>
                    <Text>score: {props.user.score}</Text>
                </View>
            </Pressable>
        </View>
    )

    /** open the github page of the user */
    function openGithubProfile() {
        WebBrowser.openBrowserAsync(
            props.user.html_url
        );
    }
    
}

/* component styles */
const styles = StyleSheet.create({
    /** github user card styles */
    githubUserCard: {
        width: 220,
        height: 90,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#c1c1c19c',
        borderRadius: 10,
        margin: 10,
        padding: 10
    },
    /* avatar container */
    avatarContainer: {
        width: '30%'
    },
    /* profile avatar */
    avatar: {
        width: 50,
        height:50,
        backgroundColor: '#c1c1c19c',
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#c1c1c19c'
    },
    /* info container */
    infoContainer: {
        width: '70%',
        marginLeft: 10
    },
    /** user login */
    userLogin: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    /** user type */
    userType: {
        fontSize: 14,
        fontWeight: '400'
    }
});