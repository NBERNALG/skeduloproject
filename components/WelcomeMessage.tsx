import * as WebBrowser from 'expo-web-browser';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';
import { MonoText } from './StyledText';

import { Text, View } from './Themed';

export default function WelcomeMessage() {
  return (
    <View style={styles.container}>
      {/* title */}
      <Text style={styles.title}>Hi, this is Nicolas Bernal technical test to Front End dev role in Skedulo company</Text>
      {/* skedulos landing page CTA */}
      <View
          style={styles.linksContainer}
          darkColor="rgba(255,255,255,0.05)"
          lightColor="rgba(0,0,0,0.05)"
          >
            {/* company link */}
            <TouchableOpacity
              onPress={() => handleLinkButton('https://skedulo.com')}
              accessibilityLabel="open Skedulos page in this link"
            >
              <MonoText style={styles.link}>https://skedulo.com</MonoText>
            </TouchableOpacity>
            {/* profile link */}
            <TouchableOpacity
              onPress={() => handleLinkButton('https://jobcity.web.app/nico-bernal')}
              accessibilityLabel="open developer profile page in this link"
            >
              <MonoText style={styles.link}><MonoText style={styles.link}>https://jobcity.web.app/nico-bernal</MonoText></MonoText>
            </TouchableOpacity>
        </View>
      {/* separator line */}
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

    </View>
  );

  /* open Skedulo landing page */
  function handleLinkButton(link: string) {
    WebBrowser.openBrowserAsync(
      link
    );
  }
}

/* component styles */
const styles = StyleSheet.create({
  /* component container */
  container: {
    width: '100%',
    alignItems: 'center',
    backgroundColor: Colors['primaryText'].text,
    padding: 50,
    paddingBottom: 10
  },
  /* title */
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#FFF'
  },
  /** links container */
  linksContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    margin: 10
  },
  /* link */
  link: {
    borderRadius: 5,
    paddingHorizontal: 4,
    marginVertical: 7,
    backgroundColor: '#FFF',
    padding: 3,
    margin: 10
  },
  /* separator */
  separator: {
    marginVertical: 10,
    height: 1,
    width: '100%',
  }
});
