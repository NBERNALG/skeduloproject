import { Dimensions } from 'react-native';
/* contains the dimensions of the app container */
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
/* export the dimensions and other metadata */
export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
};
