import { FontAwesome } from '@expo/vector-icons';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import { useEffect, useState } from 'react';

/* use the cache resources to improve start time */
export default function useCachedResources() {
  // set loading validation to false
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          ...FontAwesome.font,
          'space-mono': require('../assets/fonts/SpaceMono-Regular.ttf'),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        // set loading validation to true
        setLoadingComplete(true);
        // hide the splash screen
        SplashScreen.hideAsync();
      }
    }
    // call the function to start
    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}
